<script language="c#" runat="server">
public string QuerySelector { get; set; }
public bool ShowIcon {get;set;}
</script>

<script>
(function DnnFlash() {
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
      var c = ca[i].trim();
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
  }

  var flash, alert, notice;
  notice = {name: 'DnnFlashNoticeMessage', message: getCookie('DnnFlashNoticeMessage'), classname: 'success', icon: 'fi-check'};
  alert  = {name: 'DnnFlashAlertMessage',  message: getCookie('DnnFlashAlertMessage'),  classname: 'alert',   icon: 'fi-alert'};
  flash  = [alert, notice];

  for(var i=0; i < flash.length; i++) {
    var f = flash[i];
    if (f.message) {
      var content, alert, icon, text, close;            
      content = document.querySelector('<%= this.QuerySelector %>');
      alert   = document.createElement('div');
      icon    = document.createElement('i');
      text    = document.createTextNode(" "+f.message);
      close   = document.createElement('a');

      alert.setAttribute('class', f.classname+' alert-box radius animated fadeInDown');
      alert.setAttribute('style', 'margin-top: 20px;');
      alert.setAttribute('data-alert', '');

      <% if(this.ShowIcon) { %>
        icon.setAttribute('class', f.icon);
      <% } %>
      close.setAttribute('class', 'close');
      close.setAttribute('onclick', 'this.parentNode.parentNode.removeChild(this.parentNode);return false;');
      close.innerHTML = '&times;';

      alert.appendChild(icon);
      alert.appendChild(text);
      alert.appendChild(close);
      content.insertBefore(alert, content.firstChild);     
      document.cookie = f.name+"=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  }
}).call(this);
</script>