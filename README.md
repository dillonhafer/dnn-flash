﻿DNN Flash
===========
***

About
-----

This is a simple DNN7 Class that add simple flash messaging support. It requires Foundation Icons 3.

Setup
-----

1. Copy the Flash.ascx to your DNN install.

2. Copy the Flash.cs to your Module Solution.

3. Include Flash in your theme.

```
<%@ Register TagPrefix="bw" TagName="FLASH" Src="~/DesktopModules/Flash/Flash.ascx" %>

<bw:FLASH runat="server" QuerySelector="#main > .row" ShowIcon="false" />
```

4. Call Flash in your module's code behind.

```csharp
Flash.Notice("Product Added");
Flash.Alert("Product Destroyed");
```

## Credits

DNN Flash is maintained by [Dillon Hafer](http://www.dillonhafer.com)
## License

DNN Flash is Copyright © 2014 Dillon Hafer. It is free software, and may be redistributed under the terms specified below.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.