﻿using System;
using DotNetNuke.Entities.Modules;
using System.Web;

public class Flash : PortalModuleBase
{
    private static void SetFlash(string type, string message)
    {
        var notice = new HttpCookie(type);
        notice.Value = message;
        notice.Expires = DateTime.Now.AddDays(1d);
        notice.HttpOnly = false;
        HttpContext.Current.Response.Cookies.Add(notice);
    }

    /// <summary>
    /// This is commonly used to display error messages.
    /// Sets a flash message of type 'alert'.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static void Alert(string message)
    {
        SetFlash("DnnFlashAlertMessage", message);
    }

    /// <summary>
    /// This is commonly used to display information or success messages.
    /// Sets a flash message of type 'notice'.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static void Notice(string message)
    {
        SetFlash("DnnFlashNoticeMessage", message);
    }
}
